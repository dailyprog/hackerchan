var isMenuOpen = false;
function openMenu() {
    var menu = document.getElementsByClassName("sidebar")[0];
    menu.style.display = 'block';
}
function closeMenu() {
    var menu = document.getElementsByClassName("sidebar")[0];
    menu.style.display = 'none';
}

function toggleMenu() {
    isMenuOpen = !isMenuOpen;
    if (isMenuOpen) {
        openMenu();
    }
    else {
        closeMenu();
    }
}

var prevWindowWidth = document.documentElement.clientWidth;
function onResizeWindow() { //bugfix: <= makes it transition exactly at 700
    if (document.documentElement.clientWidth > 700 && prevWindowWidth <= 700) {
        isMenuOpen = true;
        openMenu();
        // document.getElementById("logo").src = "./resources/logo_2-text.png";
    }
    else if (document.documentElement.clientWidth < 700 && prevWindowWidth >= 700) {
        isMenuOpen = false;
        closeMenu();
        // document.getElementById("logo").src = "./resources/logo_2.png";
    }

    prevWindowWidth = document.documentElement.clientWidth;
}

function setHeaderPic() {
    /* hook for when the page first loads, make sure we have the right pic at the top */
    if (document.documentElement.clientWidth > 700)
        document.getElementById("logo").src = "./resources/logo_2-text.png";
    else
        document.getElementById("logo").src = "./resources/logo_2.png";
}

window.addEventListener("resize", onResizeWindow);
// setHeaderPic();
